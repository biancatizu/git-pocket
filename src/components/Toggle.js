import * as React from "react";
import { Switch, View, Text } from "react-native";
import { useTheme } from "../contexts/ThemeContext";

export const Toggle = () => {
  const { colors, setScheme, isDark } = useTheme();

  const toggleScheme = () => {
    isDark ? setScheme("light") : setScheme("dark");
  };

  return (
    <View
      style={{
        backgroundColor: colors.background,
        border: "none",
        paddingLeft: 8,
        paddingTop: 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <Switch value={isDark} onValueChange={toggleScheme} />
      <Text style={{ fontSize: 22, paddingLeft: 8 }}>
        {isDark ? "🌕" : "☀️"}
      </Text>
    </View>
  );
};
