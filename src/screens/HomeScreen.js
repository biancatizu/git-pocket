import React from "react";
import { View, Button, Text, TextInput } from "react-native";
import Styles from "../styles/Styles";
import { useNavigation } from "@react-navigation/native";
import { useTheme } from "../contexts/ThemeContext";
import { Toggle } from "../components/Toggle";

const HomeScreen = () => {
  const { navigate } = useNavigation();
  const { colors } = useTheme();

  const [username, setUsername] = React.useState("");
  const [warning, setWarning] = React.useState("");

  const handleSubmit = () => {
    try {
      fetch(`https://api.github.com/users/${username}/repos?type=all`)
        .then((response) => response.json())
        .then((result) => {
          console.log(result);
          if (result.message === "Not Found" || !result.length) {
            setWarning("NotFound");
          } else {
            navigate("Repositories", {
              names: result
                .sort((a, b) => a.stargazers_count - b.stargazers_count)
                .reverse()
                .map(({ name, id }) => ({ name, id }))
                .slice(0, 10),
              username,
            });
            setWarning("");
          }
        });
    } catch (error) {
      console.error(error);
    }
  };

  const { viewContainer, textTitle } = Styles;
  const themedViewContainer = {
    ...viewContainer,
    backgroundColor: colors.background,
  };
  const themedTextTitle = { ...textTitle, color: colors.color };

  return (
    <>
      <Toggle />
      <View style={themedViewContainer}>
        <Text style={themedTextTitle}>GIT POCKET</Text>
        <TextInput
          style={Styles.userInput}
          onChange={(e) => {
            setUsername(e.nativeEvent.text);
          }}
          placeholder={"Enter your github username:"}
          value={username}
        />
        <Button
          style={Styles.repoButton}
          title="Find most successful repository"
          type="outline"
          onPress={handleSubmit}
        />
        {warning === "NotFound" && (
          <Text style={{ color: "red" }}>
            User doesn't exist or has no repositories
          </Text>
        )}
      </View>
    </>
  );
};

export default HomeScreen;
