import React from "react";
import { Text, View, FlatList } from "react-native";
import Styles from "../styles/Styles";
import { Card, Paragraph } from "react-native-paper";

import { useTheme } from "../contexts/ThemeContext";

const RepoScreen = ({ route }) => {
  const { colors } = useTheme();

  const { viewContainer, textTitle, tableCard } = Styles;
  const themedViewContainer = {
    ...viewContainer,
    backgroundColor: colors.background,
  };
  const themedTextTitle = { ...textTitle, color: colors.color };

  const renderItem = ({ item }) => (
    <Card style={tableCard}>
      <Card.Content style={{ backgroundColor: colors.primary }}>
        <Paragraph style={{ color: colors.color }}>{item.name}</Paragraph>
      </Card.Content>
    </Card>
  );

  return (
    <View style={themedViewContainer}>
      <Text
        style={themedTextTitle}
      >{`${route.params.username}'s Repositories`}</Text>
      <FlatList
        data={route.params.names}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};
export default RepoScreen;
