import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  viewContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    padding: 10,
  },
  textTitle: {
    fontWeight: "200",
    fontSize: 25,
  },
  userInput: {
    backgroundColor: "#f1f1f1",
    width: "60%",
    paddingHorizontal: 15,
    paddingVertical: 15,
    margin: 15,
    alignSelf: "center",
  },
  repoButton: {
    borderColor: "black",
    borderWidth: 1,
    justifyContent: "center",
  },
  tableCard: {
    margin: 10,
  },
});

export default styles;
