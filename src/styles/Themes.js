export const LightTheme = {
  background: "#FFFFFF",
  primary: "#f1f1f1",
  color: "#121212",
};

// Dark theme colors
export const DarkTheme = {
  background: "#121212",
  color: "#fff",
  primary: "#222222",
  text: "#FFFFFF",
};
