<h1 align="center">GitHub Search</h1>

<p>A simple GitHub search app with React Native, that uses GitHub API to gather information</p>

<h3 align="center">How to use it?</h3>
<p>In the homepage, insert a GitHub username and submit it.</p>

<!-- ![](assets/homepage.png) -->
<img src="assets/homepage.png" width="300" height="600">

<p>Then you will see a page with a list of up to 10 repositories ordered by highest star count to lowest of repositories the user contributes to.</p>

<img src="assets/repos-list.png" width="300" height="600">


<p>Also, you have the option to toggle between light and dark mode.</p>

<h3 align="center">Project setup</h3>
<ul>
    <li>npm install</li>
    <li>npm start</li>
</ul>

<h3 align="center">Technologies</h3>
<p>Project is created with <b>React Native</b> and Expo CLI and integrates GitHub API with Fetch API.</p>

<h3 align="center">Inspiration</h3>
<p>Project substitute an exercise for an interview.</p>
