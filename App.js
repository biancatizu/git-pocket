import "react-native-gesture-handler";
import React from "react";
import { AppearanceProvider } from "react-native-appearance";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import HomeScreen from "./src/screens/HomeScreen";
import RepoScreen from "./src/screens/RepoScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
    <AppearanceProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Repositories" component={RepoScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </AppearanceProvider>
  );
}
